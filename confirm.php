<?php 
  session_start();
  $username = $_SESSION["username"];
  $gender = $_SESSION["gender"];
  $khoa = $_SESSION["khoa"];
  $address = $_SESSION["address"];
  $birthday = $_SESSION["birthday"];
  $file = $_SESSION["file"];
  // print_r($file)
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="./style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
</head>
<body>
  <form method="post" action="">
    <!-- username -->
    <div class="title">
      <label class="label" for="username">Họ và tên</label><br>
      <p><?php echo $username ?></p> <br>
    </div>

    <!-- gender -->
    <div class="title">
      <label class="label" for="gender">Giới tính</label><br>
      <div class="gender" name="gender" id="gender">
      <p><?php echo $gender ?></p>
      </div>
    </div>

    <!-- khoa -->
    <div class="title">
      <label class="label" for="khoa">Phân khoa</label><br>
      <p><?php echo $khoa ?></p>
    </div>

    <!-- birthday -->
    <div class="title">
      <label class="label" for="birthday">Ngày sinh</label><br>
      <p><?php echo $birthday ?></p><br>
    </div>

    <!-- address -->
    <div class="title">
      <label class="label" for="address">Địa chỉ</label><br>
      <p><?php echo $address ?></p><br>
    </div>

    <!-- image -->
    <div class="title">
      <label class="label" for="address">Hình ảnh</label><br>
      <div>
        <img src="<?php echo $file;?>" alt="" width="150">
      </div>
    </div>

    <!-- button submit -->
    <button type="submit" name="submit">Xác nhận</button>
  </form>
</body>
</html>
